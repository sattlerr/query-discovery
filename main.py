from discovery_bu_multidim import bu_discovery_multidim
from sample_multidim import MultidimSample
from discovery_bu_pts_multidim import discovery_bu_pts_multidim



sample = MultidimSample(["m;b;1; t;a;1; t;a;1;", "m;b;1; m;a;1; m;a;0;", "m;a;0; t;a;1; m;b;0;"])
result_dict1 = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=False, max_query_length=10)
print(result_dict1['queryset'])
print(f'Generalized Queries Dictionary:')
for key in result_dict1['generalized queries']:
    print(f'{key}: {result_dict1["generalized queries"][key]}')


sample = MultidimSample(["a; a; a; a;"])
result_dict2 = bu_discovery_multidim(sample, 1.0, 'smarter', domain_seperated=False, max_query_length=10)
print(result_dict2['queryset'])
print(f'Generalized Queries Dictionary:')
for key in result_dict2['generalized queries']:
    print(f'{key}: {result_dict2["generalized queries"][key]}')